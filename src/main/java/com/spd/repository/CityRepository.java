package com.spd.repository;

import com.spd.entity.City;
import com.spd.entity.Country;
import com.spd.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {

    List<City> findFirst10ByStateAndNameLike(State state, String s);

    //@Query("SELECT ci FROM City ci JOIN ci.state sta JOIN sta.country cou WHERE cou.id = :idCountry AND ci.name LIKE :cityName")
    @Query(nativeQuery = true, value = "SELECT co.NAME country, st.NAME state, ci.NAME city\n" +
            "FROM COUNTRY co\n" +
            "  INNER JOIN STATE st ON co.ID = st.COUNTRY_ID\n" +
            "  INNER JOIN CITY ci ON st.ID = ci.STATE_ID\n" +
            "WHERE co.ID = :idCountry AND ci.NAME LIKE :cityName\n" +
            "LIMIT 10")
    List<City> findFirst10ByCountryAndNameLike(Integer idCountry, String cityName);

}
