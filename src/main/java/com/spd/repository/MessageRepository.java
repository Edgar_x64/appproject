package com.spd.repository;

import com.spd.entity.Conversation;
import com.spd.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    public List<Message> findByConversationOrderByIdDesc(Conversation conversation);
}
