package com.spd.repository;

import com.spd.entity.BookingRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRequestRepository extends JpaRepository<BookingRequest, Integer> {

    List<BookingRequest> findByAnnouncementIdAndActiveTrue(Integer idAnnouncement);

    BookingRequest findOneByIdAndUserId(Integer id, Integer idUser);

}
