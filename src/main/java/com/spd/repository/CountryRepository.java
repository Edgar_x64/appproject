package com.spd.repository;

import com.spd.entity.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

    List<Country> findByNameLike(String country);

    List<Country> findByName(String country);

    Optional<Country> findOneByName(String country);

    List<Country> findFirst10ByName(String country);

    /*Page<Country> findByName(String country, PageRequest pageRequest);*/

}
