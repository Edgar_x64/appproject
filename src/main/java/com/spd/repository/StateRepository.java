package com.spd.repository;

import com.spd.entity.Country;
import com.spd.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

    List<State> findFirst10ByCountryAndNameLike(Country country, String s);

    Optional<State> findOneByName(String state);

}
