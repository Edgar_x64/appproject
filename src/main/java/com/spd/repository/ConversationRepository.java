package com.spd.repository;

import com.spd.entity.Announcement;
import com.spd.entity.Conversation;
import com.spd.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Integer> {

    List<Conversation> findByAttenders_User(User user);

    @Query("select c  from Conversation c " +
            "join c.attenders a1 " +
            "join c.attenders a2 " +
            "where a1.user = :attender1 AND a2.user = :attender2 AND c.announcement = :announcement")
    Conversation findForTwoAttenders(@Param("attender1") User attender1, @Param("attender2") User attender2, @Param("announcement") Announcement announcement);

}
