package com.spd.repository;

import com.spd.bean.FacilityBean;
import com.spd.bean.FilterBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FilterRepository {

    private final EntityManager entityManager;

    @Autowired
    public FilterRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Integer> findByFilterBean(FilterBean filterBean) {

        Map<String, Object> queryParams = new HashMap<>();

        String queryScript = "SELECT\n" +
                "   an.ID\n" +
                " FROM ANNOUNCEMENT an\n";

        if (filterBean.getCity().isPresent() ||
                filterBean.getCountry().isPresent() ||
                filterBean.getRegion().isPresent()) {
            queryScript += "INNER JOIN ADDRESS ad\n" +
                    "     ON an.ADDRESS_ID = ad.ID\n";
        }
        if (filterBean.getToPrice().isPresent() ||
                filterBean.getFromPrice().isPresent()) {
            queryScript += "INNER JOIN PRICE pr\n" +
                    "     ON an.ID = pr.ANNOUNCEMENT_ID\n";
        }
        if (filterBean.getOnlyWithImages().isPresent()) {
            queryScript += "LEFT JOIN ANNOUNCEMENT_IMAGE ai\n" +
                    "     ON an.ID = ai.ANNOUNCEMENT_ID\n";
        }
        if (filterBean.getFacilities().isPresent()) {
            queryScript += "LEFT JOIN ANNOUNCEMENT_FACILITY af\n" +
                    "     ON an.ID = af.ANNOUNCEMENT_ID\n";
        }
        if (filterBean.getPopulars().isPresent()) {
            queryScript += "LEFT JOIN FAVORITE f\n" +
                    "     ON an.ID = f.ANNOUNCEMENT_ID\n";
        }

        queryScript += "WHERE\n" +
                "   an.HIDDEN = FALSE AND an.ACTIVE = TRUE\n";

        if (filterBean.getRooms().isPresent()) {
            queryScript += "AND\n" +
                    "   an.ROOM >= :room\n";
            queryParams.put("room", filterBean.getRooms().get());
        }

        if (filterBean.getLivingPlaces().isPresent()) {
            queryScript += "AND\n" +
                    "   an.LIVING_PLACES >= :livingPlaces\n";
            queryParams.put("livingPlaces", filterBean.getLivingPlaces().get());
        }

        if (filterBean.getRegion().isPresent()) {
            queryScript += "AND\n" +
                    "   ad.REGION = :region\n";
            queryParams.put("region", filterBean.getRegion().get());
        }

        if (filterBean.getCountry().isPresent()) {
            queryScript += "AND\n" +
                    "   ad.COUNTRY = :country\n";
            queryParams.put("country", filterBean.getCountry().get());
        }

        if (filterBean.getCity().isPresent()) {
            queryScript += "AND\n" +
                    "   ad.CITY = :city\n";
            queryParams.put("city", filterBean.getCity().get());
        }

        if (filterBean.getFromPrice().isPresent()) {
            queryScript += "AND\n" +
                    "   pr.PRICE >= :fromPrice\n";
            queryParams.put("fromPrice", filterBean.getFromPrice().get());
        }

        if (filterBean.getToPrice().isPresent()) {
            queryScript += "AND\n" +
                    "   pr.PRICE <= :toPrice\n";
            queryParams.put("toPrice", filterBean.getToPrice().get());
        }

        if (filterBean.getOnlyWithImages().isPresent()) {
            queryScript += "AND\n" +
                    "   ai.ANNOUNCEMENT_ID IS NOT NULL\n";
        }

        if (filterBean.getFacilities().isPresent()) {
            queryScript += "AND\n" +
                    "   af.FACILITY_ID IN (:listFacilities)\n";
            List<FacilityBean> facilityBeans = filterBean.getFacilities().get();
            List<Integer> listIntegers = new ArrayList<>();
            facilityBeans.forEach(facilityBean -> listIntegers.add(facilityBean.getId()));
            queryParams.put("listFacilities", listIntegers);
        }

        queryScript += "GROUP BY an.ID\n";

        if (filterBean.getFacilities().isPresent()) {
            queryScript += "HAVING COUNT(DISTINCT af.ID) = :countFacility\n";
            List<FacilityBean> facilityBeans = filterBean.getFacilities().get();
            Integer countFacility = facilityBeans.size();
            queryParams.put("countFacility", countFacility);
        }

        if (filterBean.getPopulars().isPresent()) {
            queryScript += "ORDER BY COUNT(DISTINCT f.ID) DESC\n";
        }
        else {
            queryScript += "ORDER BY an.CREATED_DATE DESC\n";
        }

        queryScript += ";";

        //System.out.println(queryScript);

        Query query = entityManager.createNativeQuery(queryScript);
        queryParams.forEach(query::setParameter);
        List resultQuery = query.getResultList();

        List<Integer> result = new ArrayList<>();
        resultQuery.forEach(o -> result.add((Integer) o));

        return result;
    }

    // TODO:
    // create class builder for filter query
    private class FilterQuery {

        private String query;

        private FilterQuery() {

        }
    }

}
