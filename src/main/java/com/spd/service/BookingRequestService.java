package com.spd.service;

import com.spd.bean.BookingRequestBean;
import com.spd.bean.CalendarBookingRequestBean;
import com.spd.bean.FilterBean;
import com.spd.entity.Announcement;
import com.spd.entity.BookingRequest;
import com.spd.entity.User;
import com.spd.enums.PriceType;
import com.spd.exception.DateException;
import com.spd.repository.BookingRequestRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.spd.util.DateHelper.isIntersectionDates;

@Service
public class BookingRequestService {

    private final MapperFacade mapperFacade;
    private final SimpleDateFormat simpleDateFormat;

    private final BookingRequestRepository bookingRequestRepository;

    private final UserService userService;
    private final AnnouncementService announcementService;

    private final CheckService checkService;

    @Autowired
    public BookingRequestService(BookingRequestRepository bookingRequestRepository, MapperFacade mapperFacade, SimpleDateFormat simpleDateFormat, UserService userService, AnnouncementService announcementService, CheckService checkService) {
        this.bookingRequestRepository = bookingRequestRepository;
        this.mapperFacade = mapperFacade;
        this.simpleDateFormat = simpleDateFormat;
        this.userService = userService;
        this.announcementService = announcementService;
        this.checkService = checkService;
    }

    public List<CalendarBookingRequestBean> getBookingRequest(Integer idAnnouncement) {
        List<BookingRequest> bookingRequests = bookingRequestRepository.findByAnnouncementIdAndActiveTrue(idAnnouncement);
        return mapperFacade.mapAsList(bookingRequests, CalendarBookingRequestBean.class);
    }

    public BookingRequestBean postBookingRequest(String email, Integer idAnnouncement, BookingRequestBean bookingRequestBean) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getById(idAnnouncement);

        List<BookingRequest> bookingRequests = bookingRequestRepository.findByAnnouncementIdAndActiveTrue(idAnnouncement);

        String beanCheckIn = bookingRequestBean.getCheckIn();
        String beanCheckOut = bookingRequestBean.getCheckOut();

        checkService.checkDatesBookingRequest(beanCheckIn, beanCheckOut, bookingRequests);

        BookingRequest bookingRequest = mapperFacade.map(bookingRequestBean, BookingRequest.class);
        bookingRequest.setAnnouncement(announcement);
        bookingRequest.setUser(user);

        BookingRequest saved = bookingRequestRepository.save(bookingRequest);

        return mapperFacade.map(saved, BookingRequestBean.class);
    }

    public void approvedBookingRequest(String email, Integer idAnnouncement, BookingRequestBean bookingRequestBean) {
        announcementService.getAnnouncementByUserAndId(email, idAnnouncement);

        List<BookingRequest> bookeds = bookingRequestRepository.findByAnnouncementIdAndActiveTrue(idAnnouncement);

        //checkService.checkDatesBookingRequest(bookingRequestBean.getCheckIn(), bookingRequestBean.getCheckOut(), bookeds);

        String checkIn = bookingRequestBean.getCheckIn();
        String checkOut = bookingRequestBean.getCheckOut();

        Date dateIn;
        Date dateOut;

        try {
            dateIn = simpleDateFormat.parse(checkIn);
        } catch (ParseException e) {
            throw new DateException("Parse check in date error");
        }
        try {
            dateOut = simpleDateFormat.parse(checkOut);
        } catch (ParseException e) {
            throw new DateException("Parse check out date error");
        }

        for (BookingRequest booked : bookeds) {
            if (booked.getId().equals(bookingRequestBean.getId())) {
                continue;
            }
            Date bookedCheckIn = booked.getCheckIn();
            Date bookedCheckOut = booked.getCheckOut();
            Boolean isApproved = booked.getApproved();
            if (isIntersectionDates(bookedCheckIn, bookedCheckOut, dateIn, dateOut)) {
                if (isApproved) {
                    throw new DateException("Apartment is occupied on these dates");
                }
                booked.setActive(false);
                bookingRequestRepository.save(booked);
            }
        }

        BookingRequest bookingRequest = bookingRequestRepository.findOne(bookingRequestBean.getId());
        if (Optional.ofNullable(bookingRequestBean.getApproved()).isPresent()) {
            bookingRequest.setApproved(bookingRequestBean.getApproved());
        }
        if (Optional.ofNullable(bookingRequestBean.getComment()).isPresent()) {
            bookingRequest.setComment(bookingRequestBean.getComment());
        }
        try {
            bookingRequest.setCheckIn(simpleDateFormat.parse(bookingRequestBean.getCheckIn()));
        } catch (ParseException e) {
            throw new DateException("Parse check in date error");
        }
        try {
            bookingRequest.setCheckOut(simpleDateFormat.parse(bookingRequestBean.getCheckOut()));
        } catch (ParseException e) {
            throw new DateException("Parse check out date error");
        }
        bookingRequest.setActive(true);

        bookingRequestRepository.save(bookingRequest);
    }

    public void deleteBookingRequest(Integer id) {
        BookingRequest bookingRequest = bookingRequestRepository.findOne(id);
        bookingRequest.setActive(false);
        bookingRequestRepository.save(bookingRequest);
    }

    public List<Integer> filterDates(List<Integer> ids, FilterBean filterBean) {
        if (!filterBean.getCheckOut().isPresent()) {
            return ids;
        }

        Date dateIn;
        Date dateOut;

        if (!filterBean.getCheckIn().isPresent()) {
            dateIn = new Date();
        }
        else {
            String checkIn = filterBean.getCheckIn().get();
            try {
                dateIn = simpleDateFormat.parse(checkIn);
            } catch (ParseException e) {
                throw new DateException("Parse check in date error");
            }
        }

        String checkOut = filterBean.getCheckOut().get();
        try {
            dateOut = simpleDateFormat.parse(checkOut);
        } catch (ParseException e) {
            throw new DateException("Parse check out date error");
        }

        // TODO
        Optional<String> typePriceOptional = filterBean.getTypePrice();
        if (typePriceOptional.isPresent()) {
            if (typePriceOptional.get().equals(PriceType.PER_DAY.getType())) {

            }
            else if (typePriceOptional.get().equals(PriceType.PER_MONTH.getType())) {

            }
        }
        else {

        }

        List<Integer> result = new ArrayList<>();
        ids.forEach(idAnnouncement -> {
            List<BookingRequest> bookingRequests = bookingRequestRepository
                    .findByAnnouncementIdAndActiveTrue(idAnnouncement);
            boolean isIntersection = false;
            for (BookingRequest booked : bookingRequests) {
                Date bookedCheckIn = booked.getCheckIn();
                Date bookedCheckOut = booked.getCheckOut();
                if (isIntersectionDates(bookedCheckIn, bookedCheckOut, dateIn, dateOut)) {
                    isIntersection = true;
                    break;
                }
            }
            if (!isIntersection) {
                result.add(idAnnouncement);
            }
        });

        return result;
    }
}
