package com.spd.service;

import com.spd.bean.AnnouncementAllBean;
import com.spd.bean.AnnouncementBean;
import com.spd.bean.AnnouncementIdentifiedBean;
import com.spd.entity.Announcement;
import com.spd.entity.User;
import com.spd.exception.NoSuchAnnouncementException;
import com.spd.repository.AnnouncementRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnnouncementService {

    private final UserService userService;
    private final PaginationService paginationService;

    private final MapperFacade mapperFacade;
    private final AnnouncementRepository announcementRepository;

    @Autowired
    public AnnouncementService(AnnouncementRepository announcementRepository, MapperFacade mapperFacade, UserService userService, PaginationService paginationService) {
        this.announcementRepository = announcementRepository;
        this.mapperFacade = mapperFacade;
        this.userService = userService;
        this.paginationService = paginationService;
    }

    public List<AnnouncementAllBean> getAnnouncementsByUserEmail(String email, Optional<Integer> offsetOptional, Optional<Integer> limitOptional) {
        User user = userService.getByEmail(email);
        List<Announcement> announcements = announcementRepository.findByUserIdAndActiveTrue(user.getId());

        announcements.sort((o1, o2) -> (o1.getCreatedDate().after(o2.getCreatedDate()) ? -1 : 1));

        List<Announcement> announcementsWithPagination = paginationService.filter(announcements, offsetOptional, limitOptional);
        return mapperFacade.mapAsList(announcementsWithPagination, AnnouncementAllBean.class);
    }

    public void getAnnouncementByUserAndId(String email, int id) {
        User user = userService.getByEmail(email);
        Announcement announcement = getByIdAndUserId(id, user.getId());
    }

    public AnnouncementAllBean getAnnouncement(Integer id) {
        Announcement announcement = announcementRepository.findOne(id);
        return mapperFacade.map(announcement, AnnouncementAllBean.class);
    }

    public Announcement getById(int id) {
        return announcementRepository.findOneById(id)
                .orElseThrow(() -> new NoSuchAnnouncementException("Not such announcement"));
    }

    public AnnouncementIdentifiedBean createAnnouncement(String email, AnnouncementBean announcementBean) {
        User user = userService.getByEmail(email);

        Announcement announcement = mapperFacade.map(announcementBean, Announcement.class);
        announcement.setActive(true);
        announcement.setUser(user);
        Announcement newAnnouncement = announcementRepository.save(announcement);

        return mapperFacade.map(newAnnouncement, AnnouncementIdentifiedBean.class);
    }

    public void saveAnnouncement(Announcement announcement) {
        announcementRepository.save(announcement);
    }

    public void updateAnnouncement(String email, AnnouncementIdentifiedBean announcementIdentifiedBean) {
        User user = userService.getByEmail(email);

        Announcement announcement = announcementRepository.findOneById(announcementIdentifiedBean.getId())
                .get();
        if (announcementIdentifiedBean.getHidden() != null) {
            announcement.setHidden(announcementIdentifiedBean.getHidden());
        }
        announcement.setLivingPlaces(announcementIdentifiedBean.getLivingPlaces());
        announcement.setRoom(announcementIdentifiedBean.getRoom());
        announcement.setShortDescription(announcementIdentifiedBean.getShortDescription());
        announcement.setTitle(announcementIdentifiedBean.getTitle());
        announcementRepository.save(announcement);
    }

    public void deleteAnnouncement(String email, int id) {
        User user = userService.getByEmail(email);

        Announcement announcement = getByIdAndUserId(id, user.getId());

        announcement.setActive(false);

        announcementRepository.save(announcement);
    }

    public Announcement getByIdAndUserId(int id, int idUser) {
        return announcementRepository.findOneByIdAndUserId(id, idUser)
                .orElseThrow(() -> new NoSuchAnnouncementException("User does not have such an announcement"));
    }

    public List<Announcement> getListByIds(List<Integer> ids) {
        return announcementRepository.findAll(ids);
    }

    public void deleteCascade(Integer id, String email) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementRepository.findOneByIdAndUserId(id, user.getId())
                .orElseThrow(() -> new NoSuchAnnouncementException("No such announcement"));
        announcement.setActive(false);
        announcementRepository.save(announcement);
    }

    public void hiddenAnnouncement(String email, Integer id) {
        User user = userService.getByEmail(email);
        Optional<Announcement> announcementOptional = announcementRepository.findOneByIdAndUserId(id, user.getId());
        if (announcementOptional.isPresent()) {
            Announcement announcement = announcementOptional.get();
            Boolean hidden = announcement.getHidden();
            hidden = !hidden;
            announcement.setHidden(hidden);
            announcementRepository.save(announcement);
        }
    }
}
