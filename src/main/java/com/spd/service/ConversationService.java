package com.spd.service;

import com.spd.bean.ConversationBean;
import com.spd.builders.ConversationBeanBuilder;
import com.spd.entity.Announcement;
import com.spd.entity.Attender;
import com.spd.entity.Conversation;
import com.spd.entity.User;
import com.spd.repository.AttenderRepository;
import com.spd.repository.ConversationRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConversationService {

    private final ConversationRepository conversationRepository;
    private final AttenderRepository attenderRepository;
    private final UserService userService;
    private final MapperFacade mapperFacade;

    @Autowired
    public ConversationService(ConversationRepository conversationRepository,
                               AttenderRepository attenderRepository,
                               UserService userService,
                               MapperFacade mapperFacade) {
        this.conversationRepository = conversationRepository;
        this.attenderRepository = attenderRepository;
        this.userService = userService;
        this.mapperFacade = mapperFacade;
    }

    public Conversation saveConversation(Announcement announcement, User sender, User recipient) {

        List<User> attenders = new ArrayList<>();
        attenders.add(sender);
        attenders.add(recipient);

        Conversation forTwoAttenders = conversationRepository.findForTwoAttenders(sender, recipient, announcement);
        if (forTwoAttenders != null) {
            return forTwoAttenders;
        }

        Conversation conversation = new Conversation();
        conversation.setAnnouncement(announcement);
        conversation.setActive(true);
        conversationRepository.save(conversation);

        List<Attender> attendersList = new ArrayList<>();
        for (User attenderUser : attenders) {
            Attender attender = new Attender();
            attender.setConversation(conversation);
            attender.setUser(attenderUser);
            attenderRepository.save(attender);
            attendersList.add(attender);
        }
        conversation.setAttenders(attendersList);
        return conversation;
    }

    public List<ConversationBean> getConversations(String email) {
        User user = userService.getByEmail(email);
        List<Conversation> conversations = conversationRepository.findByAttenders_User(user);
        //return mapperFacade.mapAsList(conversations, ConversationBean.class);

        return conversations.stream().map(conversation ->
                new ConversationBeanBuilder().setConversation(conversation).setUser(user).build())
                .collect(Collectors.toList());
    }

    public Conversation getById(int id) {
        return conversationRepository.getOne(id);
    }
}
