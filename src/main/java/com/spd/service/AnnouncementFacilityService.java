package com.spd.service;

import com.spd.bean.FacilityBean;
import com.spd.entity.Announcement;
import com.spd.entity.AnnouncementFacility;
import com.spd.entity.Facility;
import com.spd.entity.User;
import com.spd.repository.AnnouncementFacilityRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnnouncementFacilityService {

    private final MapperFacade mapperFacade;
    private final UserService userService;
    private final AnnouncementService announcementService;
    private final AnnouncementFacilityRepository announcementFacilityRepository;
    private final FacilityService facilityService;

    @Autowired
    public AnnouncementFacilityService(UserService userService, AnnouncementService announcementService, FacilityService facilityService, MapperFacade mapperFacade, AnnouncementFacilityRepository announcementFacilityRepository) {
        this.userService = userService;
        this.announcementService = announcementService;
        this.mapperFacade = mapperFacade;
        this.announcementFacilityRepository = announcementFacilityRepository;
        this.facilityService = facilityService;
    }

    public List<FacilityBean> getFacilitiesByUserAndId(String email, int idAnnouncement) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());
        return getFacilityBeans(announcement);
    }

    public List<FacilityBean> getFacilityBeans(Announcement announcement) {
        List<AnnouncementFacility> announcementFacilities =
                announcementFacilityRepository.findByAnnouncementId(announcement.getId());

        List<Facility> facilities = announcementFacilities.stream()
                .map(AnnouncementFacility::getFacility)
                .collect(Collectors.toList());

        return mapperFacade.mapAsList(facilities, FacilityBean.class);
    }

    @Transactional
    public void createFacility(String email, List<FacilityBean> facilityBeans, int idAnnouncement) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());

        deleteAllAnnouncementFacilityByIdAnnouncement(idAnnouncement);

        addAllAnnouncementFacilityToAnnouncement(facilityBeans, announcement);
    }

    public void updateFacility(String email, List<FacilityBean> facilityBeans, int idAnnouncement) {
        createFacility(email, facilityBeans, idAnnouncement);
    }

    private void deleteAllAnnouncementFacilityByIdAnnouncement(int idAnnouncement) {
        List<AnnouncementFacility> announcementFacilitiesOld = announcementFacilityRepository.findByAnnouncementId(idAnnouncement);
        announcementFacilityRepository.delete(announcementFacilitiesOld);
    }

    private void addAllAnnouncementFacilityToAnnouncement(List<FacilityBean> facilityBeans, Announcement announcement) {
        List<Facility> facilities = mapperFacade.mapAsList(facilityBeans, Facility.class);

        List<AnnouncementFacility> announcementFacilitiesNew = facilities.stream()
                .map(f -> {
                    AnnouncementFacility announcementFacility = new AnnouncementFacility();
                    announcementFacility.setEnabled(true);
                    announcementFacility.setFacility(f);
                    announcementFacility.setAnnouncement(announcement);
                    return announcementFacility;
                })
                .collect(Collectors.toList());
        announcementFacilityRepository.save(announcementFacilitiesNew);
    }

    public void deleteFacility(String email, int idAnnouncement) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());

        deleteAllAnnouncementFacilityByIdAnnouncement(announcement.getId());
    }

    public void deleteFacilities(List<AnnouncementFacility> announcementFacilities) {
        announcementFacilityRepository.delete(announcementFacilities);
    }
}
