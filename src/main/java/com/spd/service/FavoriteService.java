package com.spd.service;

import com.spd.bean.*;
import com.spd.entity.Address;
import com.spd.entity.Announcement;
import com.spd.entity.Favorite;
import com.spd.entity.User;
import com.spd.repository.FavoriteRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FavoriteService {

    private final FavoriteRepository favoriteRepository;
    private final UserService userService;
    private final MapperFacade mapperFacade;
    private final AnnouncementService announcementService;
    private final AnnouncementFacilityService announcementFacilityService;
    private final PriceService priceService;
    private final PaginationService paginationService;

    @Autowired
    public FavoriteService(FavoriteRepository favoriteRepository, UserService userService, MapperFacade mapperFacade, AnnouncementService announcementService, AnnouncementFacilityService announcementFacilityService, PriceService priceService, PaginationService paginationService) {
        this.favoriteRepository = favoriteRepository;
        this.userService = userService;
        this.mapperFacade = mapperFacade;
        this.announcementService = announcementService;
        this.announcementFacilityService = announcementFacilityService;
        this.priceService = priceService;
        this.paginationService = paginationService;
    }

    public List<AnnouncementAllBean> getFavorites(String email, Optional<Integer> offsetOptional, Optional<Integer> limitOptional) {
        User user = userService.getByEmail(email);
        List<Favorite> favorites = favoriteRepository.findByUserId(user.getId());
        List<AnnouncementAllBean> announcementAllBeans = new ArrayList<>();
        favorites.forEach(favorite -> {
            AnnouncementAllBean announcementAllBean = getFavoriteBean(favorite);
            announcementAllBeans.add(announcementAllBean);
        });
        List<AnnouncementAllBean> result = paginationService.filter(announcementAllBeans, offsetOptional, limitOptional);
        return result;
    }

    private AnnouncementAllBean getFavoriteBean(Favorite favorite) {
        AnnouncementAllBean announcementAllBean = new AnnouncementAllBean();

        Announcement announcement = favorite.getAnnouncement();
        announcementAllBean.setIdAnnouncement(announcement.getId());

        AnnouncementBean announcementBean = mapperFacade.map(announcement, AnnouncementBean.class);
        announcementAllBean.setAnnouncementBean(announcementBean);

        Address address = announcement.getAddress();
        AddressBean addressBean = mapperFacade.map(address, AddressBean.class);
        announcementAllBean.setAddressBean(addressBean);

        List<FacilityBean> facilityBeans = announcementFacilityService.getFacilityBeans(announcement);
        announcementAllBean.setFacilityBeans(facilityBeans);

        List<PriceBean> priceBeans = priceService.getPricesBeans(announcement);
        announcementAllBean.setPriceBean(priceBeans.get(0)); // TODO
        return announcementAllBean;
    }

    public AnnouncementAllBean createFavorite(String email, Integer idAnnouncement) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getById(idAnnouncement);
        Favorite favorite = new Favorite();
        favorite.setUser(user);
        favorite.setAnnouncement(announcement);
        Favorite newFavorite = favoriteRepository.save(favorite);
        return getFavoriteBean(newFavorite);
    }

    public void deleteFavorite(Integer id) {
        favoriteRepository.delete(id);
    }

    public void deleteFavorites(Integer idAnnouncement) {
        List<Favorite> favorites = favoriteRepository.findByAnnouncementId(idAnnouncement);
        favoriteRepository.delete(favorites);
    }
}
