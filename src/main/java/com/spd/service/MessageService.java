package com.spd.service;

import com.spd.bean.MessageBean;
import com.spd.entity.Conversation;
import com.spd.entity.Message;
import com.spd.entity.User;
import com.spd.repository.MessageRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserService userService;
    private final MapperFacade mapperFacade;
    private final ConversationService conversationService;

    @Autowired
    public MessageService(MessageRepository messageRepository,
                          UserService userService,
                          MapperFacade mapperFacade,
                          ConversationService conversationService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
        this.mapperFacade = mapperFacade;
        this.conversationService = conversationService;
    }

    private List<Message> getMessagesForConversation(Conversation conversation) {
        return messageRepository.findByConversationOrderByIdDesc(conversation);
    }

    public void sendMessage(int conversationId, String email, String text) {
        User sender = userService.getByEmail(email);
        Conversation conversation = conversationService.getById(conversationId);
        Message message = new Message();
        message.setConversation(conversation);
        message.setText(text);
        message.setActive(true);
        message.setSender(sender);
        messageRepository.save(message);
    }

    public void markMessageRead(String email, Integer messagesId) {
        //User sender = userService.getByEmail(email);
        Message message = getById(messagesId);
        //Announcement announcement = announcementService.getByIdAndUserId(message.getConversation().getAnnouncement().getId(), sender.getId());
        message.setReceived(true);
        messageRepository.save(message);
    }

    public Message getById(int id) {
        return messageRepository.getOne(id);
    }


    public List<MessageBean> getMessages(String email, Integer conversationId) {
        User user = userService.getByEmail(email);
        Conversation conversation = conversationService.getById(conversationId);
        List<Message> messages = getMessagesForConversation(conversation);
        return mapperFacade.mapAsList(messages, MessageBean.class);
    }
}
