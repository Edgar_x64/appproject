package com.spd.service;

import com.spd.bean.AnnouncementImageBean;
import com.spd.entity.Announcement;
import com.spd.entity.AnnouncementImage;
import com.spd.entity.Image;
import com.spd.entity.User;
import com.spd.repository.AnnouncementImageRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnnouncementImageService {
    private final AnnouncementImageRepository announcementImageRepository;
    private final AnnouncementService announcementService;
    private final MapperFacade mapperFacade;
    private final UserService userService;


    @Autowired
    public AnnouncementImageService(AnnouncementImageRepository announcementImageRepository, AnnouncementService announcementService, MapperFacade mapperFacade, UserService userService) {
        this.announcementImageRepository = announcementImageRepository;
        this.announcementService = announcementService;
        this.mapperFacade = mapperFacade;
        this.userService = userService;

    }
    public void saveAnnouncementImage(Image image, String title, Integer announcementId,String email) {
        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getByIdAndUserId(announcementId, user.getId());

        AnnouncementImage announcementImage = new AnnouncementImage();
        announcementImage.setImage(image);
        announcementImage.setTitle(title);
        announcementImage.setAnnouncement(announcementService.getById(announcementId));
        announcementImage.setActive(true);
        announcementImageRepository.save(announcementImage);
    }

    public void deleteAnnouncementImage(Integer id, String email) {
        User user = userService.getByEmail(email);
        Integer idAnnouncement =  announcementImageRepository.getOne(id).getAnnouncement().getId();
        announcementService.getByIdAndUserId(idAnnouncement, user.getId());

        announcementImageRepository.delete(id);
    }

    public List<AnnouncementImageBean> getAnnouncementImages(Integer id) {
        Announcement announcement = announcementService.getById(id);
        return mapperFacade.mapAsList(announcement.getAnnouncementImages(), AnnouncementImageBean.class);
    }
}
