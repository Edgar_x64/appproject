package com.spd.service;

import com.spd.bean.ImageBean;
import com.spd.entity.Image;
import com.spd.exception.ImageException;
import com.spd.exception.NoImageException;
import com.spd.repository.ImageRepository;
import com.spd.validator.FileValidator;
import ma.glasnost.orika.MapperFacade;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageService {

    private final ImageRepository imageRepository;
    private final MapperFacade mapperFacade;

    @Autowired
    public ImageService(ImageRepository imageRepository, MapperFacade mapperFacade) {
        this.imageRepository = imageRepository;
        this.mapperFacade = mapperFacade;
    }



    public List<ImageBean> saveImages(MultipartFile[] fileImages) {
         /*      List<Image> images = Arrays.stream(fileImages)
                .map(this::saveImage)
                .collect(Collectors.toList());*/
        List<Image> images = new ArrayList<>();
        for (MultipartFile file: fileImages) {
            try {
                Image image = saveImage(file);
                images.add(image);
            } catch (ImageException e) {
                e.printStackTrace();
            }
        }
        return mapperFacade.mapAsList(images, ImageBean.class);
    }

    private Image saveImage(MultipartFile imageFile) {
        String mimeType;
        FileValidator fileValidator = new FileValidator();

        try {
            MagicMatch match = Magic.getMagicMatch(imageFile.getBytes());
            mimeType = match.getMimeType();
        } catch (Exception e) {
            mimeType = "";
        }
        fileValidator.validate(mimeType, imageFile);
        byte[] imageBytes;
        try {
            imageBytes = imageFile.getBytes();

        } catch (IOException e) {
            throw new ImageException("Bad Image data.");
        }

        Image image = new Image();
        image.setMimeType(mimeType);
        image.setData(imageBytes);
        imageRepository.save(image);
        return image;
    }

    public Image getImage(int id) {
        return imageRepository.findOne(id);
    }

    public void deleteImage(Integer deleteImage) throws Exception {
        if (deleteImage != null) {
            imageRepository.delete(deleteImage);

        } else {
            throw new NoImageException();
        }
    }
}
