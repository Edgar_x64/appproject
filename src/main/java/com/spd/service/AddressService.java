package com.spd.service;

import com.spd.bean.AddressBean;
import com.spd.entity.Address;
import com.spd.entity.Announcement;
import com.spd.entity.User;
import com.spd.exception.FilledException;
import com.spd.exception.NoSuchAddressException;
import com.spd.repository.AddressRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {

    private final AddressRepository addressRepository;
    private final AnnouncementService announcementService;
    private final UserService userService;
    private final MapperFacade mapperFacade;

    @Autowired
    public AddressService(AddressRepository addressRepository, AnnouncementService announcementService, UserService userService, MapperFacade mapperFacade) {
        this.addressRepository = addressRepository;
        this.announcementService = announcementService;
        this.userService = userService;
        this.mapperFacade = mapperFacade;
    }

    public AddressBean getAddressByUserAndAnnouncement(String email, int idAnnouncement) {
        User user = userService.getByEmail(email);

        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());

        Address address = announcement.getAddress();
        return mapperFacade.map(address, AddressBean.class);
    }

    public AddressBean createAddress(String email, AddressBean addressBean, int idAnnouncement) {
        User user = userService.getByEmail(email);

        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());

        Address address = mapperFacade.map(addressBean, Address.class);

        Optional.ofNullable(announcement.getAddress())
                .ifPresent(ignore -> new FilledException("Announcement has an address"));

        address = addressRepository.save(address);
        announcement.setAddress(address);
        announcementService.saveAnnouncement(announcement);

        return mapperFacade.map(address, AddressBean.class);
    }

    public void updateAddress(String email, AddressBean addressBean, int idAnnouncement) {
        User user = userService.getByEmail(email);

        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());
        Optional.ofNullable(announcement.getAddress())
                .orElseThrow(() -> new NoSuchAddressException("Not such address"));

        Address address = mapperFacade.map(addressBean, Address.class);
        address.setId(announcement.getAddress().getId());

        addressRepository.save(address);
    }

    public void deleteAddress(String email, int idAnnouncement) {
        User user = userService.getByEmail(email);

        Announcement announcement = announcementService.getByIdAndUserId(idAnnouncement, user.getId());
        Optional.ofNullable(announcement.getAddress())
                .orElseThrow(() -> new NoSuchAddressException("Not such address"));

        addressRepository.delete(announcement.getAddress());

        announcement.setAddress(null);
        announcementService.saveAnnouncement(announcement);
    }

    public void deleteAddress(Address address) {
        addressRepository.delete(address);
    }
}
