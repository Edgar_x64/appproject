package com.spd.service;

import com.spd.bean.AutocompleteBean;
import com.spd.bean.ListBean;
import com.spd.entity.City;
import com.spd.entity.Country;
import com.spd.entity.State;
import com.spd.enums.SearchOptional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AutocompleteService {

    private static final Integer DEFAULT_SIZE = 10;

    private final CountryService countryService;
    private final StateService stateService;
    private final CityService cityService;

    @Autowired
    public AutocompleteService(CountryService countryService, StateService stateService, CityService cityService) {
        this.countryService = countryService;
        this.stateService = stateService;
        this.cityService = cityService;
    }

    public ListBean getListWithAutocomplete(
            AutocompleteBean autocompleteBean,
            Optional<Integer> idFieldOptional) {

        if (idFieldOptional.isPresent()) {

            SearchOptional searchOptional = SearchOptional.valueOf(idFieldOptional.get());

            String country = autocompleteBean.getCountry().orElse("");
            String state = autocompleteBean.getState().orElse("");
            String city = autocompleteBean.getCity().orElse("");

            switch (searchOptional) {
                case COUNTY: {
                    if (country.equals("")) {
                        return getFirstCountry(DEFAULT_SIZE);
                    }
                    else {
                        List<Country> countries = countryService.search(country);
                        return new ListBean(
                                countries.stream().map(Country::getName).collect(Collectors.toList())
                        );
                    }
                }
                case STATE_BY_COUNTY: {
                    Optional<Country> countryOptional = countryService.getOneByName(country);
                    if (countryOptional.isPresent()) {
                        List<State> states = stateService.searchByCountry(countryOptional.get(), state);
                        return new ListBean(
                                states.stream().map(State::getName).collect(Collectors.toList())
                        );
                    }
                    else {
                        return new ListBean(Collections.emptyList());
                    }
                }
                case CITY_BY_STATE: {
                    Optional<State> stateOptional = stateService.getOneByName(state);
                    if (stateOptional.isPresent()) {
                        List<City> cities = cityService.searchByState(stateOptional.get(), city);
                        return new ListBean(
                                cities.stream().map(City::getName).collect(Collectors.toList())
                        );
                    }
                }
                case CITY_BY_COUNTRY: {
                    Optional<Country> countryOptional = countryService.getOneByName(country);
                    if (countryOptional.isPresent()) {
                        List<City> cities = cityService.searchByCountry(countryOptional.get(), city);
                        return new ListBean(
                                cities.stream().map(City::getName).collect(Collectors.toList())
                        );
                    }
                }
            }
        }

        List<String> list = new ArrayList<>();
        if (!autocompleteBean.getCountry().isPresent()) {
            return getFirstCountry(DEFAULT_SIZE);
        }
        else {
            String country = autocompleteBean.getCountry().get();
            List<Country> countries = countryService.getByName(country);
            if (countries.size() > 1) {
                return new ListBean(
                        countries.stream().map(Country::getName).collect(Collectors.toList())
                );
            }
            else {

            }
            List<Country> countriesSearch = countryService.search(country);
        }

        return new ListBean(list);
    }

    private ListBean getFirstCountry(Integer size) {
        return countryService.getListBySize(size);
    }
}
