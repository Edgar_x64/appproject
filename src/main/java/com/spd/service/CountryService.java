package com.spd.service;

import com.spd.bean.ListBean;
import com.spd.entity.Country;
import com.spd.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public ListBean getListBySize(Integer size) {
        Page<Country> countryPages = countryRepository.findAll(new PageRequest(0, size));
        List<String> list = new ArrayList<>();
        countryPages.forEach(country -> list.add(country.getName()));
        return new ListBean(list);
    }

    public List<Country> search(String country) {
        System.out.println(country);
        List<Country> result = countryRepository.findByNameLike(country + "%");
        return result;
    }

    public Optional<Country> getOneByName(String country) {
        return countryRepository.findOneByName(country);
    }

    public List<Country> getByName(String country) {
        // TODO use size
        return countryRepository.findFirst10ByName(country);
    }
}
