package com.spd.service;

import com.spd.bean.AnnouncementAllBean;
import com.spd.bean.FilterBean;
import com.spd.entity.Announcement;
import com.spd.repository.FilterRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilterService {

    private final MapperFacade mapperFacade;

    private final AnnouncementService announcementService;
    private final BookingRequestService bookingRequestService;
    private final PaginationService paginationService;


    private final FilterRepository filterRepository;

    @Autowired
    public FilterService(MapperFacade mapperFacade, AnnouncementService announcementService, BookingRequestService bookingRequestService, PaginationService paginationService, FilterRepository filterRepository) {
        this.mapperFacade = mapperFacade;
        this.announcementService = announcementService;
        this.bookingRequestService = bookingRequestService;
        this.paginationService = paginationService;
        this.filterRepository = filterRepository;
    }

    public List<AnnouncementAllBean> getAnnouncementWithFilter(FilterBean filterBean) {
        List<Integer> idsWithQueryFilter = filterRepository.findByFilterBean(filterBean);

        List<Integer> idsWithDatesFilter = bookingRequestService.filterDates(idsWithQueryFilter, filterBean);

        List<Integer> idsWithPaginableFilter = paginationService.filter(idsWithDatesFilter, filterBean.getOffset(), filterBean.getLimit());

        List<Announcement> announcements = announcementService.getListByIds(idsWithPaginableFilter);

        announcements.sort((o1, o2) -> (o1.getCreatedDate().after(o2.getCreatedDate()) ? -1 : 1));

        return mapperFacade.mapAsList(announcements, AnnouncementAllBean.class);
    }

}
