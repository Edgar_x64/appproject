package com.spd.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaginationService {

    private static final Integer DEFAULT_OFFSET = 0;
    private static final Integer DEFAULT_LIMIT = 10;

    public <T> List<T> filter(List<T> list, Optional<Integer> offsetOptional, Optional<Integer> limitOptional) {
        Integer offset = offsetOptional.orElse(DEFAULT_OFFSET);
        Integer limit = limitOptional.orElse(DEFAULT_LIMIT);
        if (offset > list.size()) {
            return new ArrayList<>();
        }
        if (limit > list.size()) {
            limit = list.size();
        }
        if (offset > limit) {
            offset = limit;
        }
        if (limit == 0) {
            return new ArrayList<>();
        }
        return list.subList(offset, limit);
    }
}
