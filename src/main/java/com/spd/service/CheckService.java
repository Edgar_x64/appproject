package com.spd.service;

import com.spd.bean.UserRegistrationBean;
import com.spd.entity.Announcement;
import com.spd.entity.BookingRequest;
import com.spd.exception.DateException;
import com.spd.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.spd.util.DateHelper.isIntersectionDates;

@Service
public class CheckService {

    private final SimpleDateFormat simpleDateFormat;

    private final AnnouncementService announcementService;

    @Autowired
    public CheckService(SimpleDateFormat simpleDateFormat, AnnouncementService announcementService) {
        this.simpleDateFormat = simpleDateFormat;
        this.announcementService = announcementService;
    }

    public void checkUserTerm(UserRegistrationBean userRegistrationBean) {
        ValidationException
                .assertTrue(userRegistrationBean.getTermsChecked(), "User shutdown exception handling");
    }

    public void checkUserOwnerAnnouncement(String email, Integer id) {
        Announcement announcement = announcementService.getById(id);
        ValidationException
                .assertTrue(announcement.getUser().getEmail().equals(email), "User not have this announcement");
    }

    public void checkDatesBookingRequest(String checkIn, String checkOut, List<BookingRequest> bookeds) {

        Date dateIn;
        Date dateOut;

        try {
            dateIn = simpleDateFormat.parse(checkIn);
        } catch (ParseException e) {
            throw new DateException("Parse check in date error");
        }
        try {
            dateOut = simpleDateFormat.parse(checkOut);
        } catch (ParseException e) {
            throw new DateException("Parse check out date error");
        }

        for (BookingRequest booked : bookeds) {
            Date bookedCheckIn = booked.getCheckIn();
            Date bookedCheckOut = booked.getCheckOut();
            Boolean isApproved = booked.getApproved();
            if (!isApproved) {
                continue;
            }
            if (isIntersectionDates(bookedCheckIn, bookedCheckOut, dateIn, dateOut)) {
                throw new DateException("Apartment is occupied on these dates");
            }
        }
    }
}
