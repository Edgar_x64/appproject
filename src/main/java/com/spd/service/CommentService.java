package com.spd.service;


import com.spd.bean.CommentBean;
import com.spd.entity.Announcement;
import com.spd.entity.Comment;
import com.spd.entity.User;
import com.spd.repository.CommentRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final UserService userService;
    private final AnnouncementService announcementService;
    private final MapperFacade mapperFacade;

    @Autowired
    public CommentService(CommentRepository commentRepository, UserService userService, AnnouncementService announcementService, MapperFacade mapperFacade) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.announcementService = announcementService;
        this.mapperFacade = mapperFacade;
    }


    public CommentBean sendComment(String email, String text, Integer announcementId) {

        User user = userService.getByEmail(email);
        Announcement announcement = announcementService.getByIdAndUserId(announcementId, user.getId());

        Comment comment = new Comment();
        comment.setText(text);
        comment.setDate(new Date());
        comment.setActive(true);
        comment.setUser(user);
        comment.setAnnouncement(announcement);

        commentRepository.save(comment);

        return mapperFacade.map(comment, CommentBean.class);
    }

    public void deleteComment(String email, Integer idComment) {

        User user = userService.getByEmail(email);
        Comment comment = commentRepository.findOne(idComment);
        announcementService.getByIdAndUserId(comment.getAnnouncement().getId(), user.getId());

        commentRepository.delete(idComment);
    }

    public void editComment(String email, String creatCommentBean, Integer idComment) {
        User user = userService.getByEmail(email);
        Comment comment = commentRepository.findOne(idComment);
        announcementService.getByIdAndUserId(comment.getAnnouncement().getId(), user.getId());

        comment.setText(creatCommentBean);
        commentRepository.save(comment);
    }
}
