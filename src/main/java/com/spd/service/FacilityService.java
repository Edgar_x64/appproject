package com.spd.service;

import com.spd.bean.FacilityBean;
import com.spd.entity.Facility;
import com.spd.repository.FacilityRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacilityService {

    private final FacilityRepository facilityRepository;
    private final MapperFacade mapperFacade;

    @Autowired
    public FacilityService(FacilityRepository facilityRepository, MapperFacade mapperFacade) {
        this.facilityRepository = facilityRepository;
        this.mapperFacade = mapperFacade;
    }

    public Facility getFacilityByTitle(String title) {
        return facilityRepository.findOneByTitle(title);
    }

    public Facility getFacilityById(Integer id) {
        return facilityRepository.findOneById(id);
    }

    public List<FacilityBean> getAllTitleFacilities() {
        List<Facility> facilities = facilityRepository.findAll();
        return mapperFacade.mapAsList(facilities, FacilityBean.class);
    }
}
