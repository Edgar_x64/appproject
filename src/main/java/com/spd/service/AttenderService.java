package com.spd.service;

import com.spd.repository.AttenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttenderService {

    private final AttenderRepository attenderRepository;

    @Autowired
    public AttenderService(AttenderRepository attenderRepository) {
        this.attenderRepository = attenderRepository;
    }

}
