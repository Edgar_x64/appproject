package com.spd.service;

import com.spd.bean.FacebookRegistrationBean;
import com.spd.bean.UserRegistrationBean;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;

@Service
public class FacebookService {

    public UserRegistrationBean createUser(Facebook facebook, FacebookRegistrationBean facebookRegistrationBean) {
        UserRegistrationBean userRegistrationBean = new UserRegistrationBean();
        userRegistrationBean.setEmail(facebook.userOperations().getUserProfile().getEmail());
        userRegistrationBean.setFirstName(facebook.userOperations().getUserProfile().getFirstName());
        userRegistrationBean.setLastName(facebook.userOperations().getUserProfile().getLastName());
        userRegistrationBean.setPassword(facebookRegistrationBean.getPassword());
        userRegistrationBean.setTermsChecked(facebookRegistrationBean.getTermsChecked());
        return userRegistrationBean;
    }
}
