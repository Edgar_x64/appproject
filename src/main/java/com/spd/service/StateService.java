package com.spd.service;

import com.spd.entity.Country;
import com.spd.entity.State;
import com.spd.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StateService {

    private final StateRepository stateRepository;

    @Autowired
    public StateService(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    public List<State> searchByCountry(Country country, String state) {
        return stateRepository.findFirst10ByCountryAndNameLike(country, state + "%");
    }

    public Optional<State> getOneByName(String state) {
        return stateRepository.findOneByName(state);
    }
}
