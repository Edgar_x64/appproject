package com.spd.service;

import com.spd.entity.City;
import com.spd.entity.Country;
import com.spd.entity.State;
import com.spd.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> searchByState(State state, String city) {
        return cityRepository.findFirst10ByStateAndNameLike(state, city + "%");
    }

    public List<City> searchByCountry(Country country, String city) {
        return cityRepository.findFirst10ByCountryAndNameLike(country.getId(), city + "%");
    }
}
