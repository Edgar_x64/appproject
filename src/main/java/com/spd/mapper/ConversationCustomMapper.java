package com.spd.mapper;

import com.spd.bean.ConversationBean;
import com.spd.bean.ConversationInfoBean;
import com.spd.entity.*;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class ConversationCustomMapper extends CustomMapper<Conversation, ConversationBean> {

    @Override
    public void mapAtoB(Conversation conversation, ConversationBean conversationBean, MappingContext context) {

        ConversationInfoBean conversationInfoBean = new ConversationInfoBean();

        Optional.ofNullable(conversation.getId())
                .ifPresent(conversationBean::setId);

        List<Integer> attenderUserIds = Optional.ofNullable(conversation.getAttenders())
                .map(List::stream).orElseGet(Stream::empty)
                .map(Attender::getUser)
                .map(User::getId)
                .collect(Collectors.toList());

        for (Message message : conversation.getMessages()) {
            conversationInfoBean.setText(message.getText());

        }
         conversationInfoBean.setFirstName(conversation.getAnnouncement().getUser().getFirstName());
         conversationInfoBean.setLastName(conversation.getAnnouncement().getUser().getLastName());
         conversationInfoBean.setEmail(conversation.getAnnouncement().getUser().getEmail());
         conversationInfoBean.setDate(conversation.getCreatedDate());
         conversationBean.setConversationInfoBean(conversationInfoBean);


        conversationBean.setAttenders(attenderUserIds);

        Optional.ofNullable(conversation.getAnnouncement())
                .map(Announcement::getId)
                .ifPresent(conversationBean::setAnnouncementId);

        Optional.ofNullable(conversation.getAnnouncement())
                .map(Announcement::getTitle)
                .ifPresent(conversationBean::setTitleAnnouncement);
    }
}
