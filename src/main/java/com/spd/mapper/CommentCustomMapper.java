package com.spd.mapper;


import com.spd.bean.CommentBean;
import com.spd.bean.CommentUserBean;
import com.spd.entity.Comment;
import com.spd.entity.User;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CommentCustomMapper extends CustomMapper<Comment, CommentBean> {

    @Override
    public void mapAtoB(Comment comment, CommentBean commentBean, MappingContext context) {
        CommentUserBean commentUserBean = new CommentUserBean();

        Optional<User> userOptional = Optional.ofNullable(comment.getUser());

        userOptional
                .map(User::getId)
                .ifPresent(commentUserBean::setIdUser);

        userOptional
                .map(User::getFirstName)
                .ifPresent(commentUserBean::setFirstName);

        userOptional
                .map(User::getLastName)
                .ifPresent(commentUserBean::setLastName);

        commentBean.setCommentUserBean(commentUserBean);

        commentBean.setId(comment.getId());
        commentBean.setText(comment.getText());
        commentBean.setDate(comment.getDate());
    }
}
