package com.spd.mapper;

import com.spd.bean.*;
import com.spd.entity.*;
import com.spd.util.UrlHelper;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AnnouncementCustomMapper extends CustomMapper<Announcement, AnnouncementAllBean> {

    private final MapperFacade mapperFacade;

    @Autowired
    public AnnouncementCustomMapper(MapperFacade mapperFacade) {
        this.mapperFacade = mapperFacade;
    }

    @Override
    public void mapAtoB(Announcement announcement, AnnouncementAllBean bean, MappingContext context) {

        bean.setIdAnnouncement(announcement.getId());

        bean.setAnnouncementBean(mapperFacade.map(announcement, AnnouncementBean.class));

        bean.setAddressBean(mapperFacade.map(announcement.getAddress(), AddressBean.class));

        if (Optional.ofNullable(announcement.getPrices()).isPresent() && announcement.getPrices().size() > 0) {
            bean.setPriceBean(mapperFacade.map(announcement.getPrices().get(0), PriceBean.class));
        }

        List<Facility> facilities = announcement.getAnnouncementFacilities().stream()
                .map(AnnouncementFacility::getFacility)
                .collect(Collectors.toList());
        bean.setFacilityBeans(mapperFacade.mapAsList(facilities, FacilityBean.class));

        List<Image> images = announcement.getAnnouncementImages().stream()
                .map(AnnouncementImage::getImage)
                .collect(Collectors.toList());

        List<ImageBean> imageBeans = new ArrayList<>();
        images.forEach(image -> {
            ImageBean imageBean = new ImageBean();
            imageBean.setId(image.getId());
            imageBean.setUrl(UrlHelper.getImageUrl(image.getId()));
            imageBeans.add(imageBean);
        });
        bean.setImageBeans(imageBeans);
        //bean.setImageBeans(mapperFacade.mapAsList(images, ImageBean.class)); // TODO

        if (Optional.ofNullable(announcement.getHidden()).isPresent()) {
            bean.getAnnouncementBean().setHidden(announcement.getHidden());
        }

    }

}
