package com.spd.mapper;

import com.spd.bean.ImageBean;
import com.spd.entity.Image;
import com.spd.util.UrlHelper;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class ImageCustomMapper extends CustomMapper<Image, ImageBean> {

    @Override
    public void mapAtoB(Image image, ImageBean imageBean, MappingContext context) {
        imageBean.setId(image.getId());
        imageBean.setUrl(UrlHelper.getImageUrl(image.getId()));

    }
}
