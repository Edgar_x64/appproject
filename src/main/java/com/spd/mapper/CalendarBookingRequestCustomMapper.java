package com.spd.mapper;

import com.spd.bean.BookingRequestBean;
import com.spd.bean.CalendarBookingRequestBean;
import com.spd.bean.UserInformationBean;
import com.spd.entity.BookingRequest;
import com.spd.entity.User;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class CalendarBookingRequestCustomMapper extends CustomMapper<BookingRequest, CalendarBookingRequestBean> {

    private final MapperFacade mapperFacade;

    private final SimpleDateFormat simpleDateFormat;

    @Autowired
    public CalendarBookingRequestCustomMapper(MapperFacade mapperFacade, SimpleDateFormat simpleDateFormat) {
        this.mapperFacade = mapperFacade;
        this.simpleDateFormat = simpleDateFormat;
    }

    @Override
    public void mapAtoB(BookingRequest bookingRequest, CalendarBookingRequestBean bean, MappingContext context) {

        //BookingRequestBean bookingRequestBean = mapperFacade.map(bookingRequest, BookingRequestBean.class);
        BookingRequestBean bookingRequestBean = new BookingRequestBean();
        bookingRequestBean.setId(bookingRequest.getId());
        bookingRequestBean.setApproved(bookingRequest.getApproved());
        bookingRequestBean.setComment(bookingRequest.getComment());
        bookingRequestBean.setCheckIn(simpleDateFormat.format(bookingRequest.getCheckIn()));
        bookingRequestBean.setCheckOut(simpleDateFormat.format(bookingRequest.getCheckOut()));

        User user = bookingRequest.getUser();
        UserInformationBean userInformationBean = mapperFacade.map(user, UserInformationBean.class);

        bean.setBookingRequestBean(bookingRequestBean);
        bean.setUserInformationBean(userInformationBean);

    }
}
