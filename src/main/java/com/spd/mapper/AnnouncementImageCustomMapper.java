package com.spd.mapper;


import com.spd.bean.AnnouncementImageBean;
import com.spd.entity.AnnouncementImage;
import com.spd.entity.Image;
import com.spd.util.UrlHelper;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AnnouncementImageCustomMapper extends CustomMapper<AnnouncementImage, AnnouncementImageBean> {

    @Override
    public void mapAtoB(AnnouncementImage image, AnnouncementImageBean bean, MappingContext mappingContext) {

        Optional.ofNullable(image.getId())
                .ifPresent(bean::setId);

        Optional.ofNullable(image.getImage())
                .map(Image::getId)
                .ifPresent(bean::setImageId);

        Optional.ofNullable(image.getTitle())
                .ifPresent(bean::setTitle);

        Optional.ofNullable(image.getImage())
                .map(Image::getId)
                .map(UrlHelper::getImageUrl)
                .ifPresent(bean::setImageUrl);
    }
}
