package com.spd.mapper;

import com.spd.bean.BookingRequestBean;
import com.spd.entity.BookingRequest;
import com.spd.exception.DateException;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

@Component
public class BookingRequestCustomMapper extends CustomMapper<BookingRequest, BookingRequestBean> {

    private final MapperFacade mapperFacade;

    private final SimpleDateFormat simpleDateFormat;

    @Autowired
    public BookingRequestCustomMapper(MapperFacade mapperFacade, SimpleDateFormat simpleDateFormat) {
        this.mapperFacade = mapperFacade;
        this.simpleDateFormat = simpleDateFormat;
    }

    @Override
    public void mapBtoA(BookingRequestBean bean, BookingRequest bookingRequest, MappingContext context) {
        try {
            bookingRequest.setCheckIn(simpleDateFormat.parse(bean.getCheckIn()));
        } catch (ParseException e) {
            throw new DateException("Parse check in date error");
        }
        try {
            bookingRequest.setCheckOut(simpleDateFormat.parse(bean.getCheckOut()));
        } catch (ParseException e) {
            throw new DateException("Parse check out date error");
        }
        bookingRequest.setComment(bean.getComment());
        if (Optional.ofNullable(bean.getApproved()).isPresent()) {
            bookingRequest.setApproved(bean.getApproved());
        }
        else {
            bookingRequest.setApproved(false);
        }
        bookingRequest.setActive(true);
    }

    @Override
    public void mapAtoB(BookingRequest bookingRequest, BookingRequestBean bean, MappingContext context) {
        bean.setId(bookingRequest.getId());
        bean.setCheckIn(simpleDateFormat.format(bookingRequest.getCheckIn()));
        bean.setCheckOut(simpleDateFormat.format(bookingRequest.getCheckOut()));
        bean.setComment(bookingRequest.getComment());
        bean.setApproved(bookingRequest.getApproved());
    }
}
