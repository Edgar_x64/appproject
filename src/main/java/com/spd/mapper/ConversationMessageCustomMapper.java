package com.spd.mapper;


import com.spd.bean.MessageBean;
import com.spd.entity.Message;
import com.spd.util.UrlHelper;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ConversationMessageCustomMapper extends CustomMapper<Message, MessageBean> {

    @Override
    public void mapAtoB(Message message, MessageBean messageBean, MappingContext context) {

        Optional.ofNullable(message.getId())
                .ifPresent(messageBean::setId);

        Optional.ofNullable(message.getCreatedDate())
                .ifPresent(messageBean::setCreatedDate);

        Optional.ofNullable(message.getUpdatedDate())
                .ifPresent(messageBean::setUpdatedDate);

        Optional.ofNullable(message.getText())
                .ifPresent(messageBean::setText);

        Optional.ofNullable(message.getReceived())
                .ifPresent(messageBean::setReceived);

        Optional.ofNullable(message.getSender().getId())
                .ifPresent(messageBean::setSenderId);

        Optional.ofNullable(message.getSender().getFirstName())
                .ifPresent(messageBean::setFirstName);

        Optional.ofNullable(message.getSender().getLastName())
                .ifPresent(messageBean::setLastName);

        Optional.ofNullable(message.getSender().getEmail())
                .ifPresent(messageBean::setEmail);

        Optional.ofNullable(message.getSender().getImage().getId())
                .ifPresent(messageBean::setImageId);

        Optional.ofNullable(UrlHelper.getImageUrl(message.getSender().getImage().getId()))
                .ifPresent(messageBean::setImageUrl);
    }
}
