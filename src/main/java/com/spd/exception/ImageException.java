package com.spd.exception;


public class ImageException extends RuntimeException {

    public ImageException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImageException(String message) {
        super(message);
    }
}
