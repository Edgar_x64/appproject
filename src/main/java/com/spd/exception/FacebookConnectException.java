package com.spd.exception;

public class FacebookConnectException extends RuntimeException {

    public FacebookConnectException(String s) {
        super(s);
    }

}
