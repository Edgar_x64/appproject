package com.spd.exception;

public class DateException extends RuntimeException {

    public DateException(String s) {
        super(s);
    }

}
