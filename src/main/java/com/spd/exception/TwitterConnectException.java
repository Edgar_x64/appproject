package com.spd.exception;

public class TwitterConnectException extends RuntimeException {

    public TwitterConnectException(String s) {
        super(s);
    }

}
