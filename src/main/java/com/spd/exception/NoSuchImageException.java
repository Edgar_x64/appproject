package com.spd.exception;


public class NoSuchImageException  extends RuntimeException{
    public NoSuchImageException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchImageException(String message) {
        super(message);
    }
}
