package com.spd.bean;

import java.util.List;

public class AnnouncementAllBean {

    private Integer idAnnouncement;
    private AnnouncementBean announcementBean;
    private AddressBean addressBean;
    private PriceBean priceBean;
    private List<FacilityBean> facilityBeans;
    private List<ImageBean> imageBeans;

    public Integer getIdAnnouncement() {
        return idAnnouncement;
    }

    public void setIdAnnouncement(Integer idAnnouncement) {
        this.idAnnouncement = idAnnouncement;
    }

    public AnnouncementBean getAnnouncementBean() {
        return announcementBean;
    }

    public void setAnnouncementBean(AnnouncementBean announcementBean) {
        this.announcementBean = announcementBean;
    }

    public AddressBean getAddressBean() {
        return addressBean;
    }

    public void setAddressBean(AddressBean addressBean) {
        this.addressBean = addressBean;
    }

    public List<FacilityBean> getFacilityBeans() {
        return facilityBeans;
    }

    public void setFacilityBeans(List<FacilityBean> facilityBeans) {
        this.facilityBeans = facilityBeans;
    }

    public List<ImageBean> getImageBeans() {
        return imageBeans;
    }

    public void setImageBeans(List<ImageBean> imageBeans) {
        this.imageBeans = imageBeans;
    }

    public PriceBean getPriceBean() {
        return priceBean;
    }

    public void setPriceBean(PriceBean priceBean) {
        this.priceBean = priceBean;
    }
}
