package com.spd.bean;

import java.util.Optional;

public class AutocompleteBean {

    private Optional<String> country;
    private Optional<String> state;
    private Optional<String> city;

    public Optional<String> getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = Optional.ofNullable(country);
    }

    public Optional<String> getState() {
        return state;
    }

    public void setState(String state) {
        this.state = Optional.ofNullable(state);
    }

    public Optional<String> getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = Optional.ofNullable(city);
    }
}
