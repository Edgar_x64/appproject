package com.spd.bean;

public class CreateConversationBean {

    private Integer announcementId;

    public Integer getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(Integer announcementId) {
        this.announcementId = announcementId;
    }
}
