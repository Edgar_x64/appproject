package com.spd.bean;

import java.util.List;

public class ConversationBean {
    private Integer id;
    private List<Integer> attenders;
    private Integer announcementId;
    private String titleAnnouncement;
    private ConversationInfoBean conversationInfoBean;

    public ConversationInfoBean getConversationInfoBean() {
        return conversationInfoBean;
    }

    public void setConversationInfoBean(ConversationInfoBean conversationInfoBean) {
        this.conversationInfoBean = conversationInfoBean;
    }

    public String getTitleAnnouncement() {
        return titleAnnouncement;
    }

    public void setTitleAnnouncement(String titleAnnouncement) {
        this.titleAnnouncement = titleAnnouncement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getAttenders() {
        return attenders;
    }

    public void setAttenders(List<Integer> attenders) {
        this.attenders = attenders;
    }

    public Integer getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(Integer announcementId) {
        this.announcementId = announcementId;
    }

}
