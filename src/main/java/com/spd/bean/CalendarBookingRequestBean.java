package com.spd.bean;

public class CalendarBookingRequestBean {

    private BookingRequestBean bookingRequestBean;
    private UserInformationBean userInformationBean;

    public BookingRequestBean getBookingRequestBean() {
        return bookingRequestBean;
    }

    public void setBookingRequestBean(BookingRequestBean bookingRequestBean) {
        this.bookingRequestBean = bookingRequestBean;
    }

    public UserInformationBean getUserInformationBean() {
        return userInformationBean;
    }

    public void setUserInformationBean(UserInformationBean userInformationBean) {
        this.userInformationBean = userInformationBean;
    }
}
