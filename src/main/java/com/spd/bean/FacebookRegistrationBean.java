package com.spd.bean;

public class FacebookRegistrationBean {

    private String password;
    private Boolean termsChecked;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getTermsChecked() {
        return termsChecked;
    }

    public void setTermsChecked(Boolean termsChecked) {
        this.termsChecked = termsChecked;
    }
}
