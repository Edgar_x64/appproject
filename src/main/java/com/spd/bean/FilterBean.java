package com.spd.bean;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class FilterBean {

    private Optional<String> country;
    private Optional<String> city;
    private Optional<String> region;
    private Optional<BigDecimal> fromPrice;
    private Optional<BigDecimal> toPrice;
    private Optional<Integer> rooms;
    private Optional<Integer> livingPlaces;
    private Optional<List<FacilityBean>> facilities;
    private Optional<Boolean> onlyWithImages;
    private Optional<Boolean> populars;
    private Optional<Integer> limit;
    private Optional<String> checkIn;
    private Optional<String> checkOut;
    private Optional<String> typePrice;
    private Optional<Integer> offset;

    public Optional<String> getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = Optional.ofNullable(country);
    }

    public Optional<String> getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = Optional.ofNullable(city);
    }

    public Optional<String> getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = Optional.ofNullable(region);
    }

    public Optional<BigDecimal> getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(BigDecimal fromPrice) {
        this.fromPrice = Optional.ofNullable(fromPrice);
    }

    public Optional<BigDecimal> getToPrice() {
        return toPrice;
    }

    public void setToPrice(BigDecimal toPrice) {
        this.toPrice = Optional.ofNullable(toPrice);
    }

    public Optional<Integer> getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = Optional.ofNullable(rooms);
    }

    public Optional<Integer> getLivingPlaces() {
        return livingPlaces;
    }

    public void setLivingPlaces(Integer livingPlaces) {
        this.livingPlaces = Optional.ofNullable(livingPlaces);
    }

    public Optional<List<FacilityBean>> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityBean> facilities) {
        this.facilities = Optional.ofNullable(facilities);
    }

    public Optional<Boolean> getOnlyWithImages() {
        return onlyWithImages;
    }

    public void setOnlyWithImages(Boolean onlyWithImages) {
        this.onlyWithImages = Optional.ofNullable(onlyWithImages);
    }

    public Optional<Boolean> getPopulars() {
        return populars;
    }

    public void setPopulars(Boolean populars) {
        this.populars = Optional.ofNullable(populars);
    }

    public Optional<Integer> getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = Optional.ofNullable(limit);
    }

    public Optional<String> getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = Optional.ofNullable(checkIn);
    }

    public Optional<String> getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = Optional.ofNullable(checkOut);
    }

    public Optional<String> getTypePrice() {
        return typePrice;
    }

    public void setTypePrice(String typePrice) {
        this.typePrice = Optional.ofNullable(typePrice);
    }

    public Optional<Integer> getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = Optional.ofNullable(offset);
    }

}
