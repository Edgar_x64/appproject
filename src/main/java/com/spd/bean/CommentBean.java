package com.spd.bean;


import java.util.Date;

public class CommentBean {
    private Integer id;
    private String text;
    private Date date;
    private CommentUserBean commentUserBean;

    public CommentUserBean getCommentUserBean() {
        return commentUserBean;
    }

    public void setCommentUserBean(CommentUserBean commentUserBean) {
        this.commentUserBean = commentUserBean;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
