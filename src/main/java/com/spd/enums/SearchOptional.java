package com.spd.enums;

public enum SearchOptional {
    COUNTY(0),
    STATE_BY_COUNTY(1),
    CITY_BY_STATE(2),
    CITY_BY_COUNTRY(3);

    private final int id;

    SearchOptional(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static SearchOptional valueOf(int i) {
        switch (i) {
            case 0:
                return COUNTY;
            case 1:
                return STATE_BY_COUNTY;
            case 2:
                return CITY_BY_STATE;
            case 3:
                return CITY_BY_COUNTRY;
            default:
                throw new RuntimeException();
        }
    }
}
