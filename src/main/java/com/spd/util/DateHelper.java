package com.spd.util;

import java.util.Date;

public class DateHelper {

    public static boolean isIntersectionDates(Date bookedCheckIn, Date bookedCheckOut, Date dateIn, Date dateOut) {
        return (afterOrEquals(bookedCheckIn, dateIn) && (beforeOrEquals(bookedCheckIn, dateOut))) ||
                (afterOrEquals(bookedCheckOut, dateIn) && beforeOrEquals(bookedCheckOut, dateOut));
    }

    private static boolean afterOrEquals(Date bookedCheckIn, Date dateIn) {
        return !bookedCheckIn.before(dateIn);
    }

    private static boolean beforeOrEquals(Date bookedCheckIn, Date dateIn) {
        return !bookedCheckIn.after(dateIn);
    }

}
