package com.spd.controller;

import com.spd.bean.BookingRequestBean;
import com.spd.bean.CalendarBookingRequestBean;
import com.spd.service.BookingRequestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/booking")
@Api(value = "booking")
public class BookingRequestController {

    private final BookingRequestService bookingRequestService;

    @Autowired
    public BookingRequestController(BookingRequestService bookingRequestService) {
        this.bookingRequestService = bookingRequestService;
    }

    @RequestMapping(value = "/{idAnnouncement}", method = RequestMethod.GET)
    @ApiOperation(value = "get booking request", httpMethod = "GET")
    public List<CalendarBookingRequestBean> getBookingRequestForAnnouncement(@PathVariable("idAnnouncement") Integer idAnnouncement) {
        return bookingRequestService.getBookingRequest(idAnnouncement);
    }

    @RequestMapping(value = "/{idAnnouncement}", method = RequestMethod.POST)
    @ApiOperation(value = "booking request", httpMethod = "POST")
    public BookingRequestBean bookingRequest(Authentication authentication, @PathVariable("idAnnouncement") Integer idAnnouncement, @RequestBody BookingRequestBean bookingRequestBean) {
        return bookingRequestService.postBookingRequest(authentication.getName(), idAnnouncement, bookingRequestBean);
    }

    @RequestMapping(value = "/{idAnnouncement}", method = RequestMethod.PUT)
    @ApiOperation(value = "booking request", httpMethod = "PUT")
    public void approvedBookingRequest(Authentication authentication, @PathVariable("idAnnouncement") Integer idAnnouncement, @RequestBody BookingRequestBean bookingRequestBean) {
        bookingRequestService.approvedBookingRequest(authentication.getName(), idAnnouncement, bookingRequestBean);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "booking request", httpMethod = "DELETE")
    public void deleteBookingRequest(Authentication authentication, @PathVariable("id") Integer id) {
        bookingRequestService.deleteBookingRequest(id);
    }

}
