package com.spd.controller;

import com.spd.bean.CommentBean;
import com.spd.bean.CreateCommentBean;
import com.spd.service.AnnouncementService;
import com.spd.service.CommentService;
import com.spd.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1/announcements/{idAnnouncement}/comments")
@Api(value = "comments")
public class CommentController {

    private final UserService userService;
    private final AnnouncementService announcementService;
    private final CommentService commentService;

    @Autowired
    public CommentController(UserService userService, AnnouncementService announcementService, CommentService commentService) {
        this.userService = userService;
        this.announcementService = announcementService;
        this.commentService = commentService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "send comment", httpMethod = "POST")
    public CommentBean sendComment(Authentication authentication, @PathVariable Integer idAnnouncement,
                                   @RequestBody CreateCommentBean createCommentBean) {
       return commentService.sendComment(authentication.getName(), createCommentBean.getText(),idAnnouncement);
    }

    @RequestMapping(value = "/{idComment}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete comment", httpMethod = "DELETE")
    public void deleteComment(Authentication authentication, @PathVariable Integer idComment) {
         commentService.deleteComment(authentication.getName(),idComment);
    }

    @RequestMapping(value = "/{idComment}", method = RequestMethod.PUT)
    @ApiOperation(value = "edit comment", httpMethod = "PUT")
    public void setUserImage(Authentication authentication , @RequestBody CreateCommentBean  createCommentBean, @PathVariable Integer idComment) {
        commentService.editComment(authentication.getName(),createCommentBean.getText(), idComment);
    }

}
