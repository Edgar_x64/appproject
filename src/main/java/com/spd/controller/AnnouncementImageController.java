package com.spd.controller;

import com.spd.bean.AnnouncementImageBean;
import com.spd.bean.AnnouncementImageCreationBean;
import com.spd.service.AnnouncementImageService;
import com.spd.service.AnnouncementService;
import com.spd.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/announcements/{id}/images")
@Api(value = "announcement images")
public class AnnouncementImageController {

    private final ImageService imageService;
    private final AnnouncementService announcementService;
    private final AnnouncementImageService announcementImageService;
    @Autowired
    public AnnouncementImageController(ImageService imageService, AnnouncementService announcementService, AnnouncementImageService announcementImageService) {
        this.imageService = imageService;
        this.announcementService = announcementService;
        this.announcementImageService = announcementImageService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "create announcement image", httpMethod = "POST")
    public void saveAnnouncementImage(Authentication authentication, @RequestBody List<AnnouncementImageCreationBean> announcementImageBeans, @PathVariable Integer id) {
        for (AnnouncementImageCreationBean announcementImageCreationBean : announcementImageBeans) {
            announcementImageService.saveAnnouncementImage(imageService.getImage(announcementImageCreationBean.getImageId()),
                    announcementImageCreationBean.getTitle(), id, authentication.getName());
        }
    }

    @RequestMapping(value = "/{announcementImageId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete announcement picture", httpMethod = "DELETE")
    public void delete(Authentication authentication,@PathVariable Integer announcementImageId)  {
        announcementImageService.deleteAnnouncementImage(announcementImageId, authentication.getName());
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "get announcement pictures", httpMethod = "GET")
    public List<AnnouncementImageBean> getAnnouncementImages(Authentication authentication, @PathVariable Integer id) {
       return announcementImageService.getAnnouncementImages(id);
    }
}
