package com.spd.controller;

import com.spd.exception.TwitterConnectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.UserOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("twitter")
public class TwitterController {

    private final Twitter twitter;
    private final ConnectionRepository connectionRepository;

    @Autowired
    public TwitterController(Twitter twitter, ConnectionRepository connectionRepository) {
        this.twitter = twitter;
        this.connectionRepository = connectionRepository;
    }

    @GetMapping
    public void helloTwitter() {
        if (connectionRepository.findPrimaryConnection(Twitter.class) == null) {
            throw new TwitterConnectException("No connection to twitter");
        }
        else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String email = authentication.getName();
            org.springframework.social.twitter.api.TwitterProfile facebookProfile =
                    twitter.userOperations().getUserProfile();
            UserOperations userOperations = twitter.userOperations();
            System.out.println();
        }
    }
}
