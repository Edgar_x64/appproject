package com.spd.controller;

import com.spd.bean.AnnouncementAllBean;
import com.spd.bean.FacilityBean;
import com.spd.bean.FilterBean;
import com.spd.service.FilterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("api/v1/filters")
@Api(value = "filters")
public class FilterController {

    private final FilterService filterService;

    @Autowired
    public FilterController(FilterService filterService) {
        this.filterService = filterService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "get announcement filter", httpMethod = "GET")
    public List<AnnouncementAllBean> getAnnouncementFilter(
            @RequestParam(required = false) String country,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String region,
            @RequestParam(required = false) BigDecimal fromPrice,
            @RequestParam(required = false) BigDecimal toPrice,
            @RequestParam(required = false) Integer rooms,
            @RequestParam(required = false) Integer livingPlaces,
            @RequestParam(required = false) List<FacilityBean> facilities,
            @RequestParam(required = false) Boolean onlyWithImages,
            @RequestParam(required = false) Boolean populars,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) String checkIn,
            @RequestParam(required = false) String checkOut,
            @RequestParam(required = false) String typePrice,
            @RequestParam(required = false) Integer offset
            ) {
        FilterBean filterBean = new FilterBean();
        filterBean.setCountry(country);
        filterBean.setCity(city);
        filterBean.setRegion(region);
        filterBean.setFromPrice(fromPrice);
        filterBean.setToPrice(toPrice);
        filterBean.setRooms(rooms);
        filterBean.setLivingPlaces(livingPlaces);
        filterBean.setFacilities(facilities);
        filterBean.setOnlyWithImages(onlyWithImages);
        filterBean.setPopulars(populars);
        filterBean.setLimit(limit);
        filterBean.setCheckIn(checkIn);
        filterBean.setCheckOut(checkOut);
        filterBean.setTypePrice(typePrice);
        filterBean.setOffset(offset);
        return filterService.getAnnouncementWithFilter(filterBean);
    }

}
