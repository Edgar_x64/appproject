package com.spd.controller.user;

import com.spd.bean.AnnouncementAllBean;
import com.spd.service.FavoriteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/users/favorites")
@Api(value = "users")
public class FavoriteController {

    private final FavoriteService favoriteService;

    @Autowired
    public FavoriteController(FavoriteService favoriteService) {
        this.favoriteService = favoriteService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "get favorites", httpMethod = "GET")
    public List<AnnouncementAllBean> getFavorites(
            Authentication authentication,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        return favoriteService.getFavorites(authentication.getName(), Optional.ofNullable(offset), Optional.ofNullable(limit));
    }

    @RequestMapping(value = "/{idAnnouncement}", method = RequestMethod.POST)
    @ApiOperation(value = "create favorite", httpMethod = "POST")
    public AnnouncementAllBean createFavorite(Authentication authentication, @PathVariable("idAnnouncement") Integer idAnnouncement) {
        return favoriteService.createFavorite(authentication.getName(), idAnnouncement);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete favorite", httpMethod = "DELETE")
    public void deleteFavorite(@PathVariable("id") Integer id) {
        favoriteService.deleteFavorite(id);
    }

}
