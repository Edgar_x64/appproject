package com.spd.controller.user.announcement;

import com.spd.bean.AnnouncementAllBean;
import com.spd.bean.AnnouncementBean;
import com.spd.bean.AnnouncementIdentifiedBean;
import com.spd.entity.Announcement;
import com.spd.service.AnnouncementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/users/announcements")
@Api(value = "announcements")
public class AnnouncementController {

    private final AnnouncementService announcementService;

    @Autowired
    public AnnouncementController(AnnouncementService announcementService) {
        this.announcementService = announcementService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "get announcements list", httpMethod = "GET")
    public List<AnnouncementAllBean> getAnnouncements(
            Authentication authentication,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset
    ) {
        List<AnnouncementAllBean> result = announcementService.getAnnouncementsByUserEmail(
                authentication.getName(),
                Optional.ofNullable(offset),
                Optional.ofNullable(limit));
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "get announcement", httpMethod = "GET")
    public AnnouncementAllBean getAnnouncement(@PathVariable("id") Integer id) {
        return announcementService.getAnnouncement(id);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "create announcement", httpMethod = "POST")
    public AnnouncementIdentifiedBean create(Authentication authentication, @RequestBody AnnouncementBean announcementBean) {
        return announcementService.createAnnouncement(authentication.getName(), announcementBean);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ApiOperation(value = "update announcement", httpMethod = "PUT")
    public void update(Authentication authentication, @RequestBody AnnouncementIdentifiedBean announcementIdentifiedBean) {
        announcementService.updateAnnouncement(authentication.getName(), announcementIdentifiedBean);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete announcement", httpMethod = "DELETE")
    public void deleteCascade(Authentication authentication, @PathVariable("id") Integer id) {
        announcementService.deleteCascade(id, authentication.getName());
    }

    @RequestMapping(value = "/hidden/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "hidden announcement", httpMethod = "PUT")
    public void create(Authentication authentication, @PathVariable Integer id) {
        announcementService.hiddenAnnouncement(authentication.getName(), id);
    }

}
