package com.spd.controller;

import com.spd.bean.FacebookRegistrationBean;
import com.spd.bean.UserInformationBean;
import com.spd.bean.UserRegistrationBean;
import com.spd.entity.User;
import com.spd.exception.FacebookConnectException;
import com.spd.service.FacebookService;
import com.spd.service.UserService;
import io.swagger.annotations.ApiOperation;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.security.SocialUser;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;

@Controller
@RequestMapping("facebook")
public class FacebookController {

    private final Facebook facebook;
    private final ConnectionRepository connectionRepository;

    private final UserService userService;
    private final FacebookService facebookService;

    private final MapperFacade mapperFacade;

    @Autowired
    public FacebookController(Facebook facebook, ConnectionRepository connectionRepository, UserService userService, FacebookService facebookService, MapperFacade mapperFacade) {
        this.facebook = facebook;
        this.connectionRepository = connectionRepository;
        this.userService = userService;
        this.facebookService = facebookService;
        this.mapperFacade = mapperFacade;
    }

    @GetMapping
    public void helloFacebook() {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            throw new FacebookConnectException("No connection to facebook");
        }
        else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String email = authentication.getName();
            org.springframework.social.facebook.api.User facebookProfile =
                    facebook.userOperations().getUserProfile();
            PagedList<Post> feed = facebook.feedOperations().getFeed();
            System.out.println();
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation(value = "registration with facebook", httpMethod = "POST")
    public void register(@RequestBody FacebookRegistrationBean facebookRegistrationBean) {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            throw new FacebookConnectException("No connection to facebook");
        }
        else {
            UserRegistrationBean userRegistrationBean = facebookService.createUser(facebook, facebookRegistrationBean);
            User user = userService.createUser(userRegistrationBean);
            user.setStatus(true);
            userService.saveUser(user);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "login with facebook", httpMethod = "POST")
    public UserInformationBean login() {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            throw new FacebookConnectException("No connection to facebook");
        }
        else {
            String email = facebook.userOperations().getUserProfile().getEmail();
            User user = userService.getByEmail(email);
            SocialUserDetails socialUserDetails = new SocialUser(
                    user.getEmail(),
                    user.getPassword(),
                    true,
                    true,
                    true,
                    true,
                    new ArrayList<>()
            );

            Authentication authentication = new UsernamePasswordAuthenticationToken(socialUserDetails, null, socialUserDetails.getAuthorities());

            Connection<Facebook> connection = connectionRepository.findPrimaryConnection(Facebook.class);
            connectionRepository.removeConnection(connection.getKey());

            SecurityContextHolder.clearContext();
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return mapperFacade.map(user, UserInformationBean.class);
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation(value = "logout facebook", httpMethod = "POST")
    public void logoutFacebook(HttpServletRequest request, Model model, Principal principal) {
        //SecurityContextHolder.clearContext();
        if (null != principal) {
            SecurityContextHolder.clearContext();

            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }

            try {
                request.logout();
            } catch (ServletException e) {
                e.printStackTrace();
            }
        }

    }

}
