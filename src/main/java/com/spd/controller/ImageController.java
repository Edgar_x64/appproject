package com.spd.controller;

import com.spd.bean.ImageBean;
import com.spd.entity.Image;
import com.spd.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/images")
@Api(value = "creating  and viewing images")
public class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "upload many images", httpMethod = "POST")
    public List<ImageBean> uploadImages(@RequestParam(value = "file") MultipartFile[] fileImages) throws Exception {
        return imageService.saveImages(fileImages);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "get image", httpMethod = "GET", produces = "image/jpeg, image/png, image/gif")
    public void getImage(@PathVariable("id") int id, HttpServletResponse response) throws IOException {
        Image image = imageService.getImage(id);
        response.setContentType(image.getMimeType());
        response.setContentLength(image.getData().length);
        response.getOutputStream().write(image.getData());
        response.getOutputStream().close();
    }

    @RequestMapping(value = "/{deleteImage}", method = RequestMethod.DELETE)
    @ApiOperation(value = "delete image", httpMethod = "DELETE")
    public void delete(@PathVariable Integer deleteImage) throws Exception {
        imageService.deleteImage(deleteImage);
    }
}
