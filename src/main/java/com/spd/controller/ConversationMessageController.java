package com.spd.controller;

import com.spd.bean.CreateMessageBean;
import com.spd.bean.MessageBean;
import com.spd.service.ConversationService;
import com.spd.service.MessageService;
import com.spd.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/messages")
@Api(value = "messages")
public class ConversationMessageController {

    private final UserService userService;
    private final ConversationService conversationService;
    private final MessageService messageService;
    private final MapperFacade mapperFacade;

    @Autowired
    public ConversationMessageController(UserService userService, ConversationService conversationService,
                                         MessageService messageService, MapperFacade mapperFacade
    ) {
        this.userService = userService;
        this.conversationService = conversationService;
        this.messageService = messageService;
        this.mapperFacade = mapperFacade;
    }

    @RequestMapping(value = "/{conversationId}/messages", method = RequestMethod.GET)
    @ApiOperation(value = "get messages", httpMethod = "GET")
    public List<MessageBean> getMessages(Authentication authentication, @PathVariable("conversationId") int conversationId) {
        return messageService.getMessages(authentication.getName(), conversationId);
    }

    @RequestMapping(value = "/{conversationId}/messages", method = RequestMethod.POST)
    @ApiOperation(value = "send message", httpMethod = "POST")
    public void sendMessage(Authentication authentication, @PathVariable("conversationId") int conversationId,
                            @RequestBody CreateMessageBean createMessageBean) {

        messageService.sendMessage(conversationId, authentication.getName(), createMessageBean.getText());
    }

    @RequestMapping(value = "/{conversationId}/messages/{messagesId}", method = RequestMethod.PUT)
    @ApiOperation(value = "mark message received", httpMethod = "PUT")
    public void markMessageReceived(Authentication authentication, @PathVariable("conversationId") int conversationId,
                                    @PathVariable("messagesId") int messagesId) {

        messageService.markMessageRead(authentication.getName(),messagesId);
    }
}
