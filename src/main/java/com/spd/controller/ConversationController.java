package com.spd.controller;

import com.spd.bean.ConversationBean;
import com.spd.bean.ConversationIdBean;
import com.spd.bean.CreateConversationBean;
import com.spd.entity.Announcement;
import com.spd.entity.Conversation;
import com.spd.entity.User;
import com.spd.service.AnnouncementService;
import com.spd.service.ConversationService;
import com.spd.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/conversations")
@Api("conversations")
public class ConversationController {

    private final ConversationService conversationService;
    private final UserService userService;
    private final AnnouncementService announcementService;
    private final MapperFacade mapperFacade;

    @Autowired
    public ConversationController(ConversationService conversationService, UserService userService,
                                  AnnouncementService announcementService, MapperFacade mapperFacade) {
        this.conversationService = conversationService;
        this.userService = userService;
        this.announcementService = announcementService;
        this.mapperFacade = mapperFacade;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "create conversation")
    public ConversationIdBean createConversation(Authentication authentication, @RequestBody CreateConversationBean conversationBean) {
        Announcement announcement = announcementService.getById(conversationBean.getAnnouncementId());
        User sender = userService.getByEmail(authentication.getName());
        User recipient = announcement.getUser();

        Conversation conversation = conversationService.saveConversation(announcement,
                sender,
                recipient);
        return mapperFacade.map(conversation, ConversationIdBean.class);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "get conversations")
    public List<ConversationBean> getConversations(Authentication authentication) {
        return conversationService.getConversations(authentication.getName());
    }
}
