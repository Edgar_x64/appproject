package com.spd.controller;

import com.spd.bean.AutocompleteBean;
import com.spd.bean.ListBean;
import com.spd.service.AutocompleteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/v1/autocomplete")
@Api(value = "autocomplete")
public class AutocompleteController {

    private final AutocompleteService autocompleteService;

    @Autowired
    public AutocompleteController(AutocompleteService autocompleteService) {
        this.autocompleteService = autocompleteService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "autocomplete", httpMethod = "GET")
    public ListBean autocomplete(
            @RequestParam(required = false) String country,
            @RequestParam(required = false) String state,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) Integer idField
    ) {
        AutocompleteBean autocompleteBean = new AutocompleteBean();
        autocompleteBean.setCountry(country);
        autocompleteBean.setState(state);
        autocompleteBean.setCity(city);
        return autocompleteService.getListWithAutocomplete(autocompleteBean, Optional.ofNullable(idField));
    }

}
