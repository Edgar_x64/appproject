package com.spd.validator;

import com.spd.exception.ImageException;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;

public class FileValidator {

    private static final int MAX_ALLOWED_FILE_SIZE = 2 * 1024 * 1024; // 2 мб
    private static final int WIDTH_BIG_FILE = 2048;
    private static final int HEIGHT_BIG_FILE = 1546;
    private static final int WIDTH_SMOLL_FILE = 200;
    private static final int HEIGHT_SMOLL_FILE = 200;


    private final String[] allowedMimeTypes = {"image/jpeg", "image/png", "image/gif"};


    public void validate(String mimeType, MultipartFile imageFile) {
        if (imageFile.getSize() > MAX_ALLOWED_FILE_SIZE) {
            throw new ImageException("File is too big.");
        }

        if (!Arrays.asList(allowedMimeTypes).contains(mimeType)) {
            throw new ImageException("Wrong mime type.");
        }

        validateImageWidthHeight(imageFile);
    }

    private void validateImageWidthHeight(MultipartFile imageFile) {

        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(imageFile.getInputStream());
        } catch (IOException e)  {
            throw new ImageException("Bad Image data.",e);
        }
        int width = bimg.getWidth();
        int height = bimg.getHeight();

        if ((width < WIDTH_SMOLL_FILE || height < HEIGHT_SMOLL_FILE) || (width > WIDTH_BIG_FILE || height > HEIGHT_BIG_FILE)) {
            throw new ImageException("Wrong Image size for width or height.");
        }
    }
}
