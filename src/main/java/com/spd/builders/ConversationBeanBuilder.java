package com.spd.builders;


import com.spd.bean.ConversationBean;
import com.spd.bean.ConversationInfoBean;
import com.spd.entity.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConversationBeanBuilder {
    private Conversation conversation;
    private User user;


    public ConversationBeanBuilder setUser(User user) {
        this.user = user;
        return this;
    }

    public ConversationBeanBuilder setConversation(Conversation conversation) {
        this.conversation = conversation;
        return this;
    }

    public ConversationBean build() {

        ConversationBean conversationBean = new ConversationBean();
        ConversationInfoBean conversationInfoBean = new ConversationInfoBean();

        Optional.ofNullable(conversation.getId())
                .ifPresent(conversationBean::setId);

        List<Integer> attenderUserIds =  Optional.ofNullable(conversation.getAttenders())
                .map(List::stream).orElseGet(Stream::empty)
                .map(Attender::getUser)
                .map(User::getId)
                .collect(Collectors.toList());

        Optional.ofNullable(conversation.getAttenders())
                .map(List::stream).orElseGet(Stream::empty)
                .map(Attender::getUser).filter(attenderUser -> !attenderUser.getId().equals(user.getId()))
                .findAny().ifPresent(foundUser-> {

            conversationInfoBean.setFirstName(foundUser.getFirstName());
            conversationInfoBean.setLastName(foundUser.getLastName());
            conversationInfoBean.setEmail(foundUser.getEmail());
        });


        int size = conversation.getMessages().size();
        if(size > 0) {
            Message message = conversation.getMessages().get(size - 1);
            conversationInfoBean.setText(message.getText());
        }

        conversationInfoBean.setDate(conversation.getCreatedDate());
        conversationBean.setConversationInfoBean(conversationInfoBean);


        conversationBean.setAttenders(attenderUserIds);

        Optional.ofNullable(conversation.getAnnouncement())
                .map(Announcement::getId)
                .ifPresent(conversationBean::setAnnouncementId);

        Optional.ofNullable(conversation.getAnnouncement())
                .map(Announcement::getTitle)
                .ifPresent(conversationBean::setTitleAnnouncement);
        return conversationBean;
    }
}
